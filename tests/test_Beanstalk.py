#!/usr/bin/env python

import common
import unittest
import time
import os
from beanstalk import Beanstalk
import boto

## enable boto logging
import logging                                                                                     
boto.set_file_logger('boto', './boto.log', level=logging.DEBUG)


class BeanstalkTestCase(unittest.TestCase):
	bs = None
	env_name = 'pyunit-test-env'
	app_name = 'pyunit-test-app'
	stack_name = '64bit Amazon Linux running Tomcat 7'
	description = 'Created via PyUnit testing of AWSAutomate!'
	cname_prefix = 'pyunit-test-app'
	region_info = None
	def setUp(self):
		""" We shall test instantiatio using credentials
		    stored in boto users .boto rc file.

		    - We use different environment name and app name
		      to make sure this is works as expected.
		    - TODO: specify ELB region as well to test attachment
		      of ELBs to our specific security groups, after creating
		      a sg to use in this PyUnit test.
		    - We default to Boto's default region for testing.
		"""
		self.region_info = boto.regioninfo.RegionInfo(None, 
			'us-west-1', 'elasticbeanstalk.us-west-1.amazonaws.com')

		self.bs = Beanstalk(region_info=self.region_info,
			env_name=self.env_name,
			app_name=self.app_name,
			stack_name=self.stack_name,
			description=self.description,
			cname_prefix=self.cname_prefix)
		print self.bs.conn
			

		
	def tearDown(self):
		""" Close the http connection to AWS api """
		self.bs.conn.terminate_environment(environment_name=self.env_name)
		self.bs.conn.delete_application(self.app_name)
		self.bs.conn.close()
	def test_checkDnsAvail_avail(self):
		self.assertTrue(self.bs.checkDnsAvail(self.cname_prefix))
	def test_checkDnsAvail_not_avail(self):
		""" Test if we indeed get a false returned when the environment
		    already exists.
		"""
		self.bs.create_application()
		self.bs.create_environment([])
		self.assertFalse(self.bs.checkDnsAvail())


if __name__ == '__main__':
	unittest.main()
