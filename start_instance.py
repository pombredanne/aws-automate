#!/usr/bin/env python

import boto

region_info = boto.regioninfo.RegionInfo(None,
			'us-west1', 'ec2.us-west1.amazonaws.com')

if __name__ == '__main__':
	ec2_connection = boto.connect_ec2(region=region_info)
	instances = ec2_connection.get_all_instances()
	print instances



