#!/usr/bin/env python

import boto
import os
import sys
import json
from bs_utils import json_print
from optparse import OptionParser
from aws_jsonconf import AWSJsonConfig
import time



class Beanstalk(object):
	'''
	An easier to use wrapper around beanstalk boto api, to enable
	encapsulation of all beanstalk required data and call
	for a complete beanstalk app and environment bootstrap.
	'''
	region_info = boto.regioninfo.RegionInfo(
		None, 'us-west-1', 'elasticbeanstalk.us-west-1.amazonaws.com')
	conn = None
	env_name = None
	app_name = None
	stack_name = None
	description = None
	aws_access_key_id = None
	aws_secret_access_key = None
	load_balancer_sg = None
	load_balancer_region_info = None
	elb_attach_giveup = 0

	

	def __init__(self, region_info=None, 
			env_name=None, 
			app_name=None, 
			stack_name=None,
			description=None,
			aws_access_key_id=None,
			aws_secret_access_key=None,
			cname_prefix=None,
			load_balancer_sg=None,
			load_balancer_region_info=None,
			elb_attach_giveup=0):
		"""
		Instantiate a Beanstalk object: Connects to the specified 
		region, retrieve all required configuration options and be 
		ready to bootstrap.
		"""
		self.region_info = region_info
		self.load_balancer_region_info=load_balancer_region_info
		self.env_name = env_name
		self.app_name = app_name
		self.stack_name = stack_name
		self.description = description
		self.aws_access_key_id = aws_access_key_id
		self.aws_secret_access_key = aws_secret_access_key
		self.cname_prefix = cname_prefix
		self.load_balancer_sg = load_balancer_sg
		self.elb_attach_giveup = elb_attach_giveup
		if self.region_info:
			self.conn = boto.connect_beanstalk(region=self.region_info,
								aws_access_key_id=self.aws_access_key_id,
								aws_secret_access_key=self.aws_secret_access_key)
		else:
			# default to AWS's default if no region specified
			# (e.g. us-east1)
			self.conn = boto.connect_beanstalk(aws_access_key_id=self.aws_access_key_id,
								aws_secret_access_key=self.aws_secret_access_key)



	def checkDnsAvail(self, cname_prefix=None):
		my_cname_prefix = cname_prefix or self.cname_prefix
		print "Checking DNS availability for: %s" % my_cname_prefix
		avail_response = self.conn.check_dns_availability(my_cname_prefix)
		dns_avail = avail_response['CheckDNSAvailabilityResponse']['CheckDNSAvailabilityResult']['Available']
		return dns_avail



	def checkApplicationExists(self, app_name=None):
		my_app_name = app_name or self.app_name
		tmp_struct = self.conn.describe_applications()
		apps_list = tmp_struct['DescribeApplicationsResponse']['DescribeApplicationsResult']['Applications']
		print "Found applications:"
		print "-------------------"
		for app in apps_list:
			print app['ApplicationName']
			if app['ApplicationName'] == my_app_name:
				return True
		return False



	def create_application(self):
		res = self.conn.create_application(self.app_name, 
					description=self.description)
		print "Create application response:"
		print "----------------------------"
		json_print(res)



	def create_environment(self, aws_opts):
		res = self.conn.create_environment(application_name=self.app_name,
							environment_name=self.env_name,
							option_settings=aws_opts,
							solution_stack_name=self.stack_name,
							cname_prefix=self.cname_prefix)
		print "Create environment response:"
		print "----------------------------"
		json_print(res)



	def describe_lbs(self):
		res = self.conn.describe_environment_resources(environment_name=self.env_name)
		self.conn.close()
		self.env_resources = res['DescribeEnvironmentResourcesResponse']['DescribeEnvironmentResourcesResult']['EnvironmentResources']
		self.load_balancers = self.env_resources['LoadBalancers']
		return self.load_balancers


	def get_first_cert(self):
		'''
		Useful if you have one cert for your IAM account, will return
		its ARN for specification in AWS option values later.
		'''
		iam_conn = boto.connect_iam()
		res = iam_conn.get_all_server_certs()
		self.cert_arn = res['list_server_certificates_response']['list_server_certificates_result']['server_certificate_metadata_list'][0]['arn']
		return self.cert_arn

	def attach_load_balancer_security_groups(self):
		load_balancers = []
		if self.load_balancer_sg:
			while (len(load_balancers)==0):
				time.sleep(3)
				print "-> Waiting for environment to have Load Balancers attached to it. Please wait..."
				# using a return value since
				# this method needs to be called every
				# interval, until an ELB has been brought up by AWS.
				load_balancers = self.describe_lbs()

			print '-> Load Balancers found.'
			elb = boto.connect_elb(region=self.load_balancer_region_info)
			for lb in load_balancers:
				elbs = map(lambda f: f.name, elb.get_all_load_balancers())
				print "elbs:",elbs
				print "load_balancers:",load_balancers
				time_elapsed = 0
				while (not lb['Name'] in elbs):
					elb.close()
					if (time_elapsed > self.elb_attach_giveup*60):
						print "!-> Giving up on attaching security group to load balancer."
						print " - Please attach the security group %s manually via the AWS Web Interface" % self.load_balancer_sg.split(',')
						print " - Load balancer name is %s" % lb['Name']
						sys.exit(1)

					print elbs
					print "-> Waiting for load balancer [%s] to be ready" % lb['Name']
					time.sleep(3)
					time_elapsed = time_elapsed + 3
					elb = boto.connect_elb(region=self.load_balancer_region_info)
					print elb
					elbs = map(lambda f: f.name, elb.get_all_load_balancers())

				# create a dictionary for easy 
				# lookup of the load balancer we want
				# to manage (since we want a reference
				# to the load balancer object).
				elb_lookup = {}
				for value in elb.get_all_load_balancers():
					elb_lookup[value.name] = value;

				for i in elbs:
					if i == lb['Name']:
						print "-> Applying sec groups %s to %s" % (
							self.load_balancer_sg, i)
						exiting_sgs = elb_lookup[i].security_groups
						new_sgs = self.load_balancer_sg.split(',')
						new_sgs.extend(exiting_sgs)
						print "-> Applying [%s] " % new_sgs
						elb_lookup[i].apply_security_groups(new_sgs)
							

					




def show_help():
	print "BeanstalkAutomate v0.9"
	print
	print "!- You didn't specify a configuration directory. This is mandatory e.g.:"
	print "   $ ./beanstalk.py CONFIG_DIR [dyanmic_params] [--app-name APP_NAME]"
	print "   		   CONFIG_DIR: A config bundle directory for BeanstalkAutomate, "
	print "   		               containing .json files for AWS options array with the namespace"
	print "   			       as the filename, and a local init.json for app and environment wide"
	print "   			       configuration."
	print "   		   APP_NAME:   Optional argument to specify an app and environment name"
	print "   			       (values will be consolidated) to override the the init.json values."
	print "				       NOTE: value length<=23 charechters."
	print
	sys.exit(1)






if __name__ == '__main__':
	if len(sys.argv)==1:
		show_help()
	# load configuration
	print "-> BeanstalkAutomate v0.9 starting..."
	try:
		awsconf = AWSJsonConfig(sys.argv[1], sys.argv)
	except Exception as e:
		print e.message
		sys.exit(1)
	# prepare a region for use, take params from init namespace
	beanstalk_region_info = boto.regioninfo.RegionInfo(None, 
			awsconf.init['BeanstalkRegion'][0], # region name
			awsconf.init['BeanstalkRegion'][1]) # endpoint name

	# prepare a region for use by the ELB connection object
	elb_region_info = boto.regioninfo.RegionInfo(None,
			awsconf.init['ElbRegion'][0],
			awsconf.init['ElbRegion'][1])


	# figure if to use AWS accessc credentials from the init.json, or
	# leave boto to use a boto rc file (.boto in the user's ~)
	print "-> Trying to load AWS access credentials from init.json"
	creds_from_init = False
	try:
		aws_access_key_id = awsconf.init['aws_access_key_id']
		aws_secret_access_key = awsconf.init['aws_secret_access_key']
		creds_from_init = True
	except:
		print "-> Could not find AWS access credentials in init.json,"
		print "   defaulting to .boto in user's home directory."
		creds_from_init = False

	# figure if to giveup after a certain timeout
	# if we could not find the ELB to attach our
	# extra security groups
	elb_attach_giveup = 0
	try:
		elb_attach_giveup = awsconf.init['ElbAttachGiveUp']
	except:
		print "!-> Must specify a minutes timeout for attaching security groups to load balancer in init.json"
		print "!- Exitting..."
		sys.exit(1)



	my_app_name = None
	my_env_name = awsconf.init['env_name']
	# assume app_name is in JSON by default
	app_name_not_in_json = False
	if 'app_name' in awsconf.init.keys():
		my_app_name = awsconf.init['app_name']
	else:
		app_name_not_in_json = True
	# EVEN IF app name is in json, allow 
	# command line arg to override it :-)
	if app_name_not_in_json or '--app-name' in sys.argv:
		print "=> Taking app_name (and environment name) from command line argument."
		try:
			my_app_name = sys.argv[sys.argv.index('--app-name')+1]
		except:
			print "!- Must specify value for --app-name. Exitting."
			sys.exit(1)

	if not my_app_name:
		print "!- Could not figure app_name (and environment hence), please specify in command line"
		print "   argument or in init.json!"
		sys.exit(1)
	else:
		my_env_name = my_app_name


	

	# clear previous log contents
	with file('boto.log', 'w') as f:
		f.truncate()

	# setup logging
	import logging
	boto.set_file_logger('boto', './boto.log', 
				level=logging.DEBUG)




	# instantiate the almighty Beanstalk object
	if not creds_from_init:
		beanstalk = Beanstalk(region_info=beanstalk_region_info,
			env_name=my_env_name,
			app_name=my_app_name,
			stack_name=awsconf.init['stack_name'],
			description=awsconf.init['description'],
			cname_prefix=my_app_name,
			load_balancer_sg=awsconf.init['LoadBalancerSecurityGroups'],
			load_balancer_region_info=elb_region_info,
			elb_attach_giveup=elb_attach_giveup)
	else:
		beanstalk = Beanstalk(region_info=beanstalk_region_info,
			env_name=my_env_name,
			app_name=my_app_name,
			stack_name=awsconf.init['stack_name'],
			description=awsconf.init['description'],
			aws_access_key_id=aws_access_key_id,
			aws_secret_access_key=aws_secret_access_key,
			cname_prefix=my_app_name,
			load_balancer_sg=awsconf.init['LoadBalancerSecurityGroups'],
			load_balancer_region_info=elb_region_info,
			elb_attach_giveup=elb_attach_giveup)


	if not beanstalk.checkDnsAvail():
		print "!- DNS not available for: %s" % (my_env_name)
		sys.exit(1)
	if beanstalk.checkApplicationExists():
		print "!- Application already exists: %s" % (my_app_name)
		sys.exit(1)
	beanstalk.create_application()
	# don't forget to pass the AWS options array
	# to Beanstalk object
	try:
		print '-> Sending environment create request.'
		beanstalk.create_environment(awsconf.options)
		print '-> Trying to attach load balancer security group.'
		beanstalk.attach_load_balancer_security_groups()
	except Exception as e:
		print e.message
		print "!- Finished in ERROR. Please check my messages and consult 'boto.log' for AWS erros." 
		sys.exit(1)

	print "-> Finished in success, you can make sure all is right using the AWS web console."
	print "   An detailed log is also available at ./boto.log"



		
	

