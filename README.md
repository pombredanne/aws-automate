# Welcome To AWSAutomate 

AWSAutomate is a complete easy to use environment to create AWS 
applications and environemts out of a standartized configuration folder structure.

# How To Use?

First, clone the repository- the repository structure is as follows:

```
#!bash

(venv)sivang@lp-sivang:~/work/Feb2013/groovy2py/boto$ tree
.
├── app_config.class
│   ├── aws:autoscaling:asg.json
│   ├── aws:autoscaling:launchconfiguration.json
│   ├── aws:ec2:vpc_internal.json
│   ├── aws:ec2:vpc_internet-facing.json
│   ├── aws:elasticbeanstalk:application:environment.json
│   ├── aws:elasticbeanstalk:container:tomcat:jvmoptions.json
│   ├── aws:elasticbeanstalk:hostmanager.json
│   ├── aws:elb:loadbalancer.json
│   ├── aws:elb:policies.json
│   └── init.json
├── aws_jsonconf.py
├── aws_xpp_dev
│   ├── aws:autoscaling:asg.json
│   ├── aws:autoscaling:launchconfiguration.json
│   ├── aws:ec2:vpc_internal.json
│   ├── aws:ec2:vpc_internet-facing.json
│   ├── aws:elasticbeanstalk:application:environment.json
│   ├── aws:elasticbeanstalk:container:tomcat:jvmoptions.json
│   ├── aws:elasticbeanstalk:hostmanager.json
│   ├── aws:elb:loadbalancer.json
│   ├── aws:elb:policies.json
│   └── init.json
├── beanstalk.py
├── bs_utils.py
├── README.md
└── requirements.txt (AWSAutomate dependencies file)

```

Then, to make sure you don't taint your system wide Python installation use the
excellent [virtualenv](https://pypi.python.org/pypi/virtualenv) , which would also
enable you to install all the required dependencies for AWSAutomate 
easily via [pip](http://www.pip-installer.org/en/latest/quickstart.html).

Now, let us setup a runtime environment with *virtualenv* and *pip*, on Ubuntu
this is quite easy:

```
#!bash

$ sudo apt-get install python-virtualenv
```

Then:

```
#!bash

$ virtualenv --no-site-packages venv
The --no-site-packages flag is deprecated; it is now the default behavior.
New python executable in venv/bin/python
Installing distribute.............................................................................................................................................................................................done.
Installing pip...............done.
```

Shell into your new virtual environment and install dependencies:

```
#!bash

$ source venv/bin/activate
(venv)$ pip install -r requirements.txt
```

The requirements.txt file is the file mentioned above cloned from the git repository.

(This will install a bunch of packages and report when done)

Yay! We are now ready to use BeanstalkAutomate to create an application environment
with the configuration of our liking as represented in our config dir. For simplicity,
there's a sample config dir provided in the repository: aws_xpp.

(For further convinience there's also a skeleton config in the *app_config.class*
directory you can use to create more configuration classes.)

For an example of how to selectively load configuration snippets based on dynamic command
line arguments, please take a look at the init.json file supplied in the aws_xpp config dir
example as well. It basically maps command line argument names to json options snippets
to be loaded into the AWS environment via BeanstalkAutomate or any other tool of the
AWSAutomate toolset.

So, where were we?...Oh! right, we were about to try and bootstrap an app environment using
BeanstalkAutomate - Fun! (don't forget to make sure you are inside the virtualenv first).

```
#!bash

$ ./beanstalk.py aws_xpp internal
```

If everything goes well, you should see no errors reported or exceptions thrown
in the air. To make sure your app and environment were indeed created, navigate
to the [AWS console](https://console.aws.amazon.com/elasticbeanstalk/home?region=us-west-1)
and from the dropdown, make sure that your app name appears and choose it.

Then you can click on *Environment Details*, and then *Edit Configuration" and all
of the options you specified in your config directory should be there.

For reference purposes, the full help of BeanstalkAutomate beanstalk.py is brought here:

```
BeanstalkAutomate v0.9

!- You didn't specify a configuration directory. This is mandatory e.g.:
   $ ./beanstalk.py CONFIG_DIR [dyanmic_params] [--app-name APP_NAME]
                   CONFIG_DIR: A config bundle directory for BeanstalkAutomate, 
                               containing .json files for AWS options array with the namespace
                               as the filename, and a local init.json for app and environment wide
                               configuration.
                   APP_NAME:   Optional argument to specify an app and environment name
                               (values will be consolidated) to override the the init.json values.
                                       NOTE: value length<=23 charechters.


```

## Congratulations! 

You have successfully used AWSAutomate's BeanstalkAutomate to create your application and environment
in AWS.
